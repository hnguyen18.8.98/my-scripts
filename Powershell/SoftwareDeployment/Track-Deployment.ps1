# Define the desired version of the application
$desiredVersion = '5.6.15'

# Path to the CSV file on the network
$csvPath = '\\path\to\csv\file'
# Try to get the version of the currently installed application
try {
    $installedVersion = (Get-WmiObject -Query "SELECT * FROM Win32_Product WHERE Name = 'NAP Locked down browser'").Version
} catch {
    # If the application is not found, $installedVersion will be null
    $installedVersion = $null
}

# Get the hostname of the current computer
$hostname = $env:COMPUTERNAME

# Check if the file exists, if not create it
if (-not (Test-Path -Path $csvPath)) {
    New-Item -ItemType File -Path $csvPath
    Add-Content -Path $csvPath -Value "Hostname"
}

# Read the existing CSV file
if ((Get-Content -Path $csvPath) -ne '') {
    $existingHostnames = Import-Csv -Path $csvPath
} else {
    $existingHostnames = @()
}

# Check if the hostname is already in the file
if ($existingHostnames.Hostname -notcontains $hostname) {
    # Check if the installed version matches the desired version
    if ($installedVersion -eq $desiredVersion) {
        # Create an object with a property named 'Hostname'
        $obj = New-Object PSObject -Property @{
            Hostname = $hostname
        }

        # Write the object to the CSV file
        $obj | Export-Csv -Path $csvPath -Append -NoTypeInformation
    }
}
