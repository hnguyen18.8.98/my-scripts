# Laptop Deployment Scripts

## Overview

This repository contains two PowerShell scripts designed to automate the deployment of the 'NAP Locked down browser' across a wide network of laptops. Tailored to update over 500+ laptops across various sites, these scripts simplify the deployment process. The updated approach uses startup/shutdown scripts.

## Scripts Description

1. **Installation Script (`Deployment.ps1`)**: This script checks the installed version of the 'NAP Locked down browser' against the desired version (5.6.15). If the version is not up-to-date or if the software is absent, it automatically initiates the installation of the correct version. The script includes popup messages to inform the user about the installation.

2. **Deployment Tracking Script (`Track-Deployment.ps1`)**: Following the installation, this script verifies the installed version and logs the hostname of each laptop in a CSV file on a network location. It ensures no duplicate entries for laptops where the correct version has been installed.

## Usage

### Prerequisites

- PowerShell should be installed and configured to run scripts.
- Network access is required for the specified paths for software installation and CSV file storage.
- Adjust the network paths and version numbers in the scripts to align with your specific environment.

### Running the Scripts

1. **Installation Script**:
   - Configure `Deployment.ps1` to run as a startup or shutdown script via Group Policy or another method.
   - The script will automatically execute at startup/shutdown, verifying the software version and proceeding with the installation if needed.

2. **Deployment Tracking Script**:
   - `Track-Deployment.ps1` should be run in conjunction with the installation script. It can be appended to the end of `Deployment.ps1` or set to run separately as a startup/shutdown script.
   - This script records the deployment status in the CSV file, noting the laptops with the correct software version.

### Notes

- The installation script communicates with users through popup messages, ensuring they are informed about the installation process.
- Deployment tracking provides a comprehensive and accurate record of the software deployment across the network.

## Contributions and Support

Your contributions and feedback are welcome. Feel free to fork this repository, submit pull requests with improvements, or open an issue for any suggestions or queries.
