# Add reference to necessary assembly for MessageBox
Add-Type -AssemblyName PresentationFramework


# Define the desired version of the application
$desiredVersion = '5.6.15'

# Try to get the version of the currently installed application
try {
    $installedVersion = (Get-WmiObject -Query "SELECT * FROM Win32_Product WHERE Name = 'NAP Locked down browser'").Version
} catch {
    # If the application is not found, $installedVersion will be null
    $installedVersion = $null
}

# Check if the installed version matches the desired version
if ($installedVersion -eq $desiredVersion) {
    # Exit the script if the desired version is already installed
    exit
}

# Execute the MSI installer with credentials
Start-Process -FilePath "msiexec.exe" -ArgumentList "/i `path\to\exe` /quiet /qn /passive" -Wait


# Wait for 5 seconds
Start-Sleep -Seconds 5

# Exit the script
Exit
