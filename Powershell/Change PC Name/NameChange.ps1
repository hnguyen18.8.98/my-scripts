﻿$hostname = read-host "Please enter your new HOSTNAME"
Write-Host "YOUR PREFERRED HOSTNAME IS $hostname - INITIALIZING HOSTNAME CHANGE"

$domaincred = Get-Credential
Rename-Computer -NewName $hostname -DomainCredential $domaincred -Restart
