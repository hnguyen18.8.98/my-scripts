
### The Great AD Cleanup Extravaganza
#### Once upon a time, a company gazed upon their Active Directory (AD) in despair, it was more tangled than a bowl of spaghetti. They needed a hero to tame this wild digital jungle. Enter me, armed with the mightiest weapon: automation!

## This task was as thrilling as watching paint dry – renaming computers and herding them into their proper Organizational Units (OU). Sure, I could've done it the old-fashioned way, with a symphony of monotonous clicks, but where's the fun in that?

# My motto? Why click when you can script? Automation for the win! 🚀🤖💻
