﻿#####################################  
 ## http://kunaludapi.blogspot.com  
 ## Version: 1  
 ## Tested this script on successfully
 ##  1) Powershell v3  
 ##  2) Windows 2012  
 ##  
 #####################################
Begin {  
   Clear-Host  
   #Check for Active Directory module  
   if (-not (Import-Module activedirectory)) {  
     Import-Module activedirectory  
   }  
   
   #Generate Random Password  
   function Generate-Password {  
     $alphabets= "abcdefghijklmnopqstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()"  
     $char = for ($i = 0; $i -lt $alphabets.length; $i++) { $alphabets[$i] }  
   
     for ($i = 1; $i -le 9; $i++)  
     {  
       $CharArray += Write-Output $(get-random $char)  
       if ($i -eq 9) {} #write-output `n  
     }  
     $CharArray  
   }  
     
   #Get AD user account and validate it  
   do {   
     $SamAccountName = Read-Host "`nReset Password For AD Account"  
   
     if ($SamAccountName -eq "") {  
       Clear-Host  
       Write-Host -Object "`nPlease type AccountName`n" -BackgroundColor Red  
       continue  
     }  
     elseif ($(Get-ADUser -LDAPFilter "(sAMAccountName=$SamAccountName)").SamAccountName -eq $SamAccountName) {  
       $AccountToReset = Get-ADUser -LDAPFilter "(sAMAccountName=$SamAccountName)"  
         
       break  
     }  
     else {  
       Clear-Host  
       Write-Host -Object "`nTyped Account Name doesn't exists, Please try again`n" -BackgroundColor Red  
       $Everything_is_fine = $false   
     }  
   }  
   while ($SamAccountName -eq "" -or $Everything_is_fine -eq $false)  
     
   Write-Host "`nAccount has been verified and it exists`n" -ForegroundColor Green  
     
   $To = Read-Host "`nTL or Manager you want to send password to[Manager@example.com]"  
   #One Time Information fillup  
   $From ="abc@yahoo.com"  
   $Subject = "Password reset request for user $SamAccountName"  
   $SmtpServer = "smtp.yahoo.com"  
   $port = 25  
 }  
   
 Process {  
   #Reset password and unlock it  
   $PlainText = Generate-Password  
   $Password = ConvertTo-SecureString -AsPlainText $PlainText -Force  
   $AccountToReset | Set-ADAccountPassword -Reset -NewPassword $Password  
   $AccountToReset | Unlock-ADAccount  
   Write-Warning "Password reseted and unlocked"  
   
   #Send Email  
   $Body = "$SamAccountName requested for New password and it is $PlainText"  
   Send-MailMessage -To $To -From $From -Subject $Subject -Body $Body -SmtpServer $SmtpServer -Port 25  
   Write-Host "Information emailed to Manager or TL" -ForegroundColor Cyan  
 }  
   
 End {  
   #Write-Host "New password is $PlainText"  
   Pause  
 }  