
# Eclectic Script Emporium
![Python](https://img.shields.io/badge/python-%2314354C.svg?&style=flat-square&logo=python&logoColor=white)
![PowerShell](https://img.shields.io/badge/powershell-%2314354C.svg?&style=flat-square&logo=powershell&logoColor=white)

Welcome to my digital trove of scripts, where my keyboard wizardry has conjured up solutions for the modern-day sorcerer (a.k.a. tech professionals). Here you'll find a smorgasbord of Python and PowerShell spells, perfect for everything from automating your coffee maker to sorting your digital sock drawer.

## Table of Contents
- [Why Are We Here?](#introduction)
- [The Scriptopedia](#scripts-overview)
- [Send a Raven (Or Just Email)](#contact)

## Why Are We Here?
Ever found yourself thinking, "There's got to be an easier way to do this?" Well, you're in luck! I've spent years stumbling into digital pitfalls and scripting my way out. This repository is my way of saying, "I survived, and you can too!"

## The Scriptopedia
Inside, you'll find scripts ranging from the "Why did I even make this?" to the "How did I live without this?" Each one is a testament to my philosophy: If it's repetitive, automate it! (Also, I was probably too lazy to do it manually more than once.)

### As the great Bill Gates might say, "I choose a lazy person to do a hard job, because they'll find an easy way to do it." That's my mantra!

## Script Survival Guide
Please, for the love of code, read the READMEs and script comments. They're there to prevent any accidental digital wizardry mishaps.

## Send a Raven (Or Just Email)
Got a script idea that's just crazy enough to work? Want to collaborate on the next big automation sensation? Let's connect:

- **LinkedIn Wizardry**: [Hoang T. Nguyen](https://www.linkedin.com/in/hoang-t-nguyen-a871541a4/)
- **Digital Pigeon Post**: [harrytnguyeen@gmail.com](mailto:harrytnguyeen@gmail.com)
