import os
rootdir = r'path_to_all_exe_file'
ext = ('.exe')

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith(ext):
            print(os.path.join(subdir, file))
